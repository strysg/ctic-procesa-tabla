def listaElementos(cadena, separador='-'):
    return cadena.split(separador)

def procesarElementoYGuardar(elemento, diccionario, distinguirMayusculas=False):
    '''Al dar un elemento, incrementa el contador de ocurrencias en el diccionario'''
    el = elemento
    if not distinguirMayusculas:
        el = el.lower()
    if diccionario.get(el) == None:
        diccionario[el] = 1
    else:
        diccionario[el] += 1
    return diccionario

def listaPeticiones(cadenaPeticiones):
    '''Procesa la cadena de peticiones recibida y devuelve una lista de elementos
    con los hosts a los que pertenecen por ejemplo:

    fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900|Open+Sans:300,400,600,700,800|Roboto:100,300,400,500,700,900-
cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.11/jquery.mousewheel.min.js?_=1559621781300-
maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js-
fonts.googleapis.com/css?family=Montserrat:regular,700,900,100,200,300

    Devuelve:

    ["fonts.googleapis.com", "cdnjs.cloudflare.com","fonts.googleapis.com" ]
    '''
    listaHttp = cadenaPeticiones.split('http://')
    listaHttps = cadenaPeticiones.split('https://')
    listaPeticiones = []
    if len(listaHttp) > 1:
        for e in listaHttp:
            # quitando espacios en blanco
            listaPeticiones.append(e.strip())
    if len(listaHttps) > 1:
        for e in listaHttps:
            # quitando espacios en blanco
            listaPeticiones.append(e.strip())
    return listaPeticiones


def listaUrlsArchivos(cadenaArchivos):
    '''Procesa la cadena de archivos recibida y devuelve un diccionario que cuenta con las extensiones de los archivos privativos doc,docx,xls,xlsx,ppt,pptx,rar, por ejemplo:
    
    - https://www.dgac.gob.bo/ran/RequisitosSGC/DRAN_REG_001R2.doc - [DOC]DGAC/DAF/EXC/SG/001/2014-1C PROVISIÓN DE PASAJES ... - www.dgac.gob.bo/contrataciones/bienes/FORM.%20PASAJES.doc - [DOC]La Paz, 18 de noviembre de 2003 - DGAC - https://www.dgac.gob.bo/daf/.../FormularioPostulacion%20DGAC.doc - [DOC]b) Es imprescindible que los Datos Altimétricos de la ... - DGAC - https://www.dgac.gob.bo/wp-content/uploads/2018/.../Form_Solicitud_Cert_Altura.do... -  - [DOC]Formulario de Solicitud de Registro y Habilitación de ... Pasantías – YPFB [XLS]Historico de Inversiones - YPFB - https://www.ypfb.gob.bo/es/transparencia/gestión-2015/.../88-inversiones.html?...794... - [XLS]Inversiones

    Devuelve:

    { 'doc': 7, 'xls': 2 }

    '''
    dic = {
        "doc": 0,
        "docx": 0,
        "xls": 0,
        "xlsx": 0,
        "ppt": 0,
        "pptx": 0,
        "rar": 0
    }
    # buscando por extension de archivo .doc .docx ... etc
    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.doc)$", re.IGNORECASE), cadenaArchivos.strip())
            if (len(res.groups()) > 0):
                dic['doc'] += 1
    except:
        pass

    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.docx)$", re.IGNORECASE), cadenaArchivos)
            if (len(res.groups()) > 0):
                dic['docx'] += 1

        res = re.match(re.compile("^(.*\.(?(docx)$))?[^.]*$", re.IGNORECASE), cadenaArchivos)
        dic['docx'] = len(res.groups())
    except:
        pass
    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.xls)$", re.IGNORECASE), cadenaArchivos)
            if (len(res.groups()) > 0):
                dic['xls'] += 1
    except:
        pass
    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.xlsx)$", re.IGNORECASE), cadenaArchivos)
            if (len(res.groups()) > 0):
                dic['xlsx'] += 1
    except:
        pass
    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.ppt)$", re.IGNORECASE), cadenaArchivos)
            if (len(res.groups()) > 0):
                dic['ppt'] += 1        
    except:
        pass
    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.pptx)$", re.IGNORECASE), cadenaArchivos)
            if (len(res.groups()) > 0):
                dic['pptx'] += 1
    except:
        pass
    try:
        for cad in cadenaArchivos.split('- '):
            # print (cad)
            res = re.match(re.compile("^.*(\.rar)$", re.IGNORECASE), cadenaArchivos)
            if (len(res.groups()) > 0):
                dic['rar'] += 1
    except:
        pass
    # buscando por etiqueta (que devuelve google)
    dic['doc'] += len(cadenaArchivos.split('[DOC]')) - 1
    dic['docx'] += len(cadenaArchivos.split('[DOCX]')) - 1
    dic['xls'] += len(cadenaArchivos.split('[XLS]')) - 1
    dic['xlsx'] += len(cadenaArchivos.split('[XLSX]')) - 1
    dic['ppt'] += len(cadenaArchivos.split('[PPT]')) - 1
    dic['pptx'] += len(cadenaArchivos.split('[PPTX]')) - 1
    dic['rar'] += len(cadenaArchivos.split('[RAR]')) - 1

    # print ('DIC:::::::::', str(dic))
    return dic
