Este script sirve para procesar el archivo `tablaSeguimientoBorrador.csv` y mostrar los resultados. Necesita python 3 y un entorno virtual o el modulo `tldextract`.

## Requisitos

Ambiente python3 con `numpy`, `pandas` y `tldextract` instalados. Los resultados se pueden revisar en el cuaderno de notas `main.ipynb` (jupyter).

```bash
python3 tabla.py
```


<!-- ## Instalacion -->

<!-- ```bash -->
<!-- git clone https://gitlab.com/strysg/ctic-procesa-tabla/tree/master -->
<!-- cd ctic-procesa-tabla -->
<!-- virtualenv --python=python3 venv -->
<!-- . venv/bin/activate -->

<!-- pip install -r requirements.txt -->

<!-- # para ejecutar -->
<!-- python main.py -->
<!-- # para guardar en un archivo -->
<!-- python main.py > resultados.md -->
<!-- ``` -->
