import numpy as np
import pandas as pd
import tldextract

#import .lib from funciones
import lib.funciones as funciones

#### programa principal #####
ruta = 'tablaSeguimiento.csv'
tt = pd.read_csv(ruta)

# filtrando solamente los analizados correctamente
tt = tt[tt['hecho'] == 's']
print('TOTAL:', str(tt.count))

# peticiones externas
lt = []
for peticion in tt['peticiones_externas']:
    if not pd.isna(peticion):
        lt = lt + funciones.listaPeticiones(peticion)
pa = pd.Series(lt)

pa = pa[pa != '']
pa = pa[pa != '-']
lt = []
for e in pa:
    elemento = tldextract.extract(e)
    cad = elemento.subdomain + '.' + elemento.domain + '.' + elemento.suffix
    lt.append(cad)
pa = pd.Series(lt)

print('peticiones externas ----------------')
print(pa.value_counts())

# servidor web
pds = pd.Series({})
for servidores in tt['servidor']:
    if pd.isna(servidores):
        continue
    elementos = funciones.listaElementos(servidores)
    for el in elementos:
        el = el.strip().lower()
        # print('--'+ str(el) + '--')
        if pds.get(el) is None:
            pds[el] = 1
        else:
            pds[el] +=1
print('servidores privativos --------------------------------')
print(pds)

# frontend
pds = pd.Series({})
for peticiones in tt['frontend']:
    if pd.isna(peticiones):
        continue
    elementos = funciones.listaElementos(peticiones)
    for el in elementos:
        el = el.strip().lower()
        # print('--'+ str(el) + '--')
        if pds.get(el) is None:
            pds[el] = 1
        else:
            pds[el] +=1
print('peticiones frontend =---------------------------')
print(pds)

# observaciones a archivos publicados
pds = pd.Series({'docx': 0, 'ppt': 0, 'doc': 0, 'rar': 0, 'xls': 0, 'xlsx': 0, 'pptx': 0})
for link in tt['obs_archivos_publicados']:
    if not pd.isna(link):
        # actualizando valores
        pds = pds.add(pd.Series(funciones.listaUrlsArchivos(link)))
        # print('>>>', str(pds))
print('observaciones archivos publicados ---------------')
print(str(pds))

# solicitudes de excepcion de software
pds = pd.Series({})
for peticiones in tt['excepciones_presentadas']:
    if pd.isna(peticiones):
        continue
    elementos = funciones.listaElementos(peticiones)
    for el in elementos:
        el = el.strip().lower()
        if pds.get(el) is None:
            pds[el] = 1
        else:
            pds[el] +=1
print('excepciones presentadas ---------------------------')
print(pds)

# solicitudes de excepcion tipos
pds = pd.Series({})
for peticiones in tt['comentario']:
    if pd.isna(peticiones):
        continue
    elementos = funciones.listaElementos(peticiones)
    for el in elementos:
        el = el.strip().lower()
        if pds.get(el) is None:
            pds[el] = 1
        else:
            pds[el] +=1
print('excepciones presentadas tipos ---------------------------')
print(pds)





    
